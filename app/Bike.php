<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bike extends Model
{
    public $table = "bikes";

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
