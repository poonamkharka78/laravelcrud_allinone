<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailModel extends Model
{
    protected $table = "details";
}
