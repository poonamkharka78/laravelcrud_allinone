<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App\Models\EmployeeDetails;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)//funtion to view data from database
    {
        $response = EmployeeDetails::paginate(5);
        $link = $response->links();
        
        return view('employee.index', compact('response','link'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employee.createEmp');
        

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'emp_name' => 'required',
            'department' => 'required',
        ],[
            'emp_name.required' => 'Please Enter Employee Name',
            'department.required' => 'Please Enter  Department',]);

        if ($validator->fails()) {
            return redirect('/create')
                        ->withErrors($validator)
                        ->withInput();
        }

        $res = new EmployeeDetails;
        $res->emp_name = $request->emp_name; //after equal to alway mention request to get value of variable.
        $res->department = $request->department;
        $data = $res->save();
        if($data){
            Session::flash('flash_message', 'Employee Record Added successfully');
            Session::flash('flash_type','alert-success');
            return Redirect('/index');
        }else{
            return  redirect('/create');
        }


}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $record = EmployeeDetails::find($id);
        return view('employee.edit',compact("record"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'emp_name' => 'required',
            'department' => 'required',
        ],
        ['emp_name.required' => 'Please Enter Employee Name',
        'department.required' => 'Please Enter  Department',
    ]
    );

        if ($validator->fails()) {
            return redirect('edit/'.$id)
                        ->withErrors($validator)
                        ->withInput();
        }

        $record = EmployeeDetails::find($id);
        $record->emp_name = $request->emp_name;
        $record->department = $request->department;
        $data = $record->save();
        if ($data) {
            Session::flash('formData', 'Employee Record Updated successfully');
            Session::flash('flash_type', 'alert-success');
            return redirect('/index');
        } else {
            return redirect('edit/'.$id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $record = EmployeeDetails::find($id);
       $r = $record->delete();
       if($r){

        Session::flash('message', 'Employee Record Removed Successfully');
        Session::flash('flash_type', 'alert-success');
        return redirect('/index');

       }else{
         return redirect('/index');
       }
    }

}
