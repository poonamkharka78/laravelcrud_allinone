<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class JoinController extends Controller
{
   public function index(){

   	$users = DB::table('emp_details')
            ->join('dapartment_details', 'emp_details.id', '=', 'dapartment_details.emp_id')
            ->select('emp_details.emp_name','dapartment_details.department','emp_details.date_of_joining')
            ->groupBy('emp_details.age')
            ->having('emp_details.age', '<', 35)
			->get();
   	
return view('joinData', ['data' => $users]);
   }


}
