<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ToDoModel;
use Session;

class TodoController extends Controller
{

	  public function index()
	  	{
	  		
	  		$records = ToDoModel::get();
	  		return view('toDo.index',compact('records'));

	  	}	
   			public function create(Request $request)
   			{

			   	$data = new ToDoModel;
			   	$data->task = $request->task;
			   	//dd($data);
			   	$storeData = $data->save(); 
			   	//dd($storeData);
			   		if($storeData){
			   			Session::flash('message','Task Added Successfully');
			   			return redirect('/todo-index');
			   			}
			   				else{
							   		Session::flash('errormsg','Something went wrong');
							   		return redirect('/todo-index');
							   	}

			}

			public function delete($id)
			{
				$record = ToDoModel::find($id);
				$del = $record->delete();
				if($del){
					return redirect('/todo-index');
				}
				else{
					return redirect('/todo-index');
				}
			} 
	}
