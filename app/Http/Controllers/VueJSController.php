<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class VueJSController extends Controller
{
   public function index()
    {

    	$countries = DB::table("mst_country")
					->select("country_name","id")
					->get();
        return view('multiField',compact('countries'));
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
           
        ]);


        return response()->json(['success'=>'Done!']);
    }
}
