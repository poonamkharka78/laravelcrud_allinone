<?php

namespace App\Http\Controllers\SubController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Collection;
use Session;

class SessionController extends Controller
{
    public function index(){
    	return view("session.index");
    }

    public function home(Request $request){
    	
    	$data = $request->session()->all();
    	return view("session.home",compact('data'));
    }

    public function store(Request $request){
    	
      $nameVal= $request->session()->push('user.name','name');
		  //$nameVal = Input::get('name');
    	$aboutVal = Input::get('about');
    	$request->session()->put('name',$nameVal);
    	$request->session()->put('about',$aboutVal);
    	$request->session()->save();
		
	    if($request){
			
	    	Session::flash('msg','Data Submit');
	    	return redirect('/session-home');
	    	}
	    	else{
	    		Session::flash('fail','Data Not Submit');
	    		return redirect('/session-home');
	    	}
    	}

   public function remove(Request $request){
   	$request->session()->forget('name');
      if($request){
	   	return redirect('/session-home');
	   }
else{
	   return redirect('/session-home');
	   }	
   } 	
}
