<?php

namespace App\Http\Controllers\SubController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MultiFieldModel;
use Illuminate\Support\Facades\Validator;


class MultifieldController extends Controller
{
	public function index(){
		return view('MultiField.index');
	}

	public function store(Request $request){

		$validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:posts',
        ]);

        if ($validator->fails()) {
            return redirect('home-page')
                        ->withErrors($validator)
                        ->withInput();
        }
		$data = new MultiFieldModel;
		$data->name = $request->name;
		$data->email = $request->email;
		$data->gst =$request->gst == true? 'Y':'N';
		$data->vat =$request->vat == true?'Y':'N';
		$store = $data->save();
		//dd($store);
		if($store){
            dd("save");
		}
		else{
			dd("not save");
		}

	}
    
}
