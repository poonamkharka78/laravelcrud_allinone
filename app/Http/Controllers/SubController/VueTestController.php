<?php

namespace App\Http\Controllers\SubController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class VueTestController extends Controller
{
    public function index(){
    	return view('VueValidation.basicTest');
    }

    public function store(Request $request){
   

    	$request->validate([
            'name' => 'required',
            'email' => 'required',
            'test'=>'required'
        ],[
        	'name.required' => 'Please Enter Name',
			'email.required' => 'Please Enter Email',
			'test.required' =>'Please Enter Any Text' ]);

        return response()->json(['status'=>'true']);
    }
}
