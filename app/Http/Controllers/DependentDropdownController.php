<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Models\DropDownVueModel;

class DependentDropdownController extends Controller
{
	public function index(){
		$countries = DB::table("mst_country")
					->select("country_name","id")
					->get();
        //return response()->json($countries);
     
        return view('multiField', compact('countries'));
	}

	public function getStateList(Request $request)
    {
    	 //dd($request->all());
        $states = DB::table("mst_states")
        			->select("state_name")
                    ->where("country_id", $request->country_id)                    
                    ->get();

       return response()->json($states);
    }

    public function save(Request $request)
    {

    //dd($request);

     $request->validate([
            'name' => 'required',
            'contactNo'=>'required|min:10|max:10',
            'gender'=> 'required',
            'address1' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'email' => 'required|email',
            'description' => 'required',
            
        ],[
          'name.required'=>'Enter Your Name',
          'contactNo.required'=>'Enter Your contact No',
          'gender.required'=>'Select Gender',
          'address1.required'=>'Enter Your Adress Detail',
          'country.required'=>'Select Country',
          'state.required'=>'Select state',
          'city.required'=>'Enter City Name',
          'email.required'=>'Enter Enail Name',
          'description.required'=>'Enter Description Name',

        ]);
     //dd($request);

       $data = new DropDownVueModel;
       $data->name = $request->name;
       $data->contactNo = $request->contactNo;
       $data->gender = $request->gender;
       //dd($data);
       $data->address1 = $request->address1;
       $data->address2 = $request->address2;
       $data->country = $request->country;
       $data->state = $request->state;
       $data->city= $request->city;
       $data->email=$request->email;
       $data->description=$request->description;
       $store = $data->save();
       dd($data);
        if($store){

            echo " save";
            //return Redirect('/customer-satisfaction-form');
            //return response()->json(['success'=>'Done!']);
        }
        else{
           echo "not save";
        }

    }
}
