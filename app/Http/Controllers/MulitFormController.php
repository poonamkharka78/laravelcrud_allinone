<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\DetailModel;
use App\Models\AddressModel;

class MulitFormController extends Controller
{

	public function index(){
		return view('multiForm');

	}
    public function save(Request $request){
		if('btnFirst'){

    		$validator = Validator::make($request->all(), [
            'f_name' => 'required',
            'l_name' => 'required',
        ],[
            'f_name.required' => 'Please Enter First Name',
            'l_name.required' => 'Please Enter Last Name',]);

        if ($validator->fails()) {
            
            return redirect('/form')
                        ->withErrors($validator)
                        ->withInput();
        	}

        	$res = new DetailModel;
        	$res->f_name = $request->f_name; //after equal to alway mention request to get value of variable.
        	$res->l_name = $request->l_name;
        	$data = $res->save();
        	//dd($data);
			}

		if ('btnsubmit'){
				
			$validator = Validator::make($request->all(), [
            'mobile' => 'required',
            'address' => 'required',
        	],[
            'mobile.required' => 'Please Enter Mobile No',
            'address.required' => 'Please Enter Address',]);

        	if($validator->fails()) {
            	return redirect('/form')
                        ->withErrors($validator)
                        ->withInput();
        	}

        	$r = new AddressModel;
        	$r->mobile = $request->mobile; //after equal to alway mention request to get value of variable.
        	$r->address = $request->address;
        	$myData = $r->save();
        	//dd($myData);
        	if($myData){
        		Session::flash('msg', 'Record Saved successfully');
            	Session::flash('flash_type', 'alert-success');
            	return Redirect('/form');
        		}

			}

		else{
			echo "No data save";
		}

    	}
	}
