<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ImageModel;
use Session;

class ImageUploadController extends Controller
{
    public function index(){

    	return view('imageUpload');
    }
  
    public function upload(Request $request){
    	request()->validate([
		'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
			],[
		 'image.required' => 'Please Upload An Image']);

    	$data = new ImageModel;
    	if($request->hasFile('image')) {
        $name = time().'.'.request()->image->getClientOriginalExtension();//file name
        //dd(time());
        request()->image->move(public_path('images'), $name);
        $data->image = $name;
      	}
    	$img = $data->save();
    	if($img){
    		Session::flash('upload','Image Uploaded');
            return redirect('/image-index')->with('image',$name);
    	}
    	else{
			Session::flash('fail','Image Not Uploaded');
    		return redirect('/image-index');
    	}
    }
}
