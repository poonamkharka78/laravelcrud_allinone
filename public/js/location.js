  
  $('#country').change(function(){
    var countryID = $(this).val();  
    //alert(countryID);  
    if(countryID){
        $.ajax({
          type:"GET",
          dataType:"json",
          url:"http://127.0.0.1:8000/state-list?country_id="+countryID,
          success:function(res){               
            if(res){
                //alert(res);
                $("#state").empty();
                $("#state").append('<option>Select State</option>');
                $.each(res,function(key,value){

                    $("#state").append('<option value="'+key.sid+'">'+ value.state_name +'</option>');
                });
           
            }else{
               $("#state").empty();
            }
           }

        });
      }     
   });