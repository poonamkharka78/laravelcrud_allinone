<?php
use Illuminate\Http\Request;
use App\Models\EmployeeDetails;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Auth::routes(['verify' => true]);

//@Eloquent Relations Routes
Route::get('/home', 'HomeController@index')->middleware('verified');
Route::get('/profile','HomeController@profile');

//@Crud routes
Route::get('/index','EmployeeController@index');
Route::get('fetch-data','EmployeeController@fetch_data');
Route::get('/create','EmployeeController@create');
Route::post('/store','EmployeeController@store');
Route::get('delete/{id}','EmployeeController@destroy');
Route::get('edit/{id}','EmployeeController@edit');
Route::post('update/{id}','EmployeeController@update');


//@join route
Route::get('/join-table','JoinController@index');

//@Multi Form Routes
Route::get('/form','MulitFormController@index');
Route::post('/storage','MulitFormController@save');


//@Multi Field Routes without vue validation
Route::get('customer-satisfaction-form','DependentDropdownController@index');
Route::get('country-list','DependentDropdownController@getCountryList');
Route::get('state-list','DependentDropdownController@getStateList');
Route::post('save-single-customer-data','DependentDropdownController@save');

//@Redirection routes
Route::get('/admin', function(){
    return view('redirection.admin');
})->middleware('admin');
 
 
Route::get('/customer', function(){
    return view('redirection.customer');
})->middleware('customer');


//@To Do list routes
Route::get('todo-index','TodoController@index');
Route::post('todo-store','TodoController@create');
Route::get('delete-task/{id}','TodoController@delete');

//@Image upload Route
Route::get('image-index','ImageUploadController@index')->name('test');//name is for rounting and grouping
Route::post('upload-image','ImageUploadController@upload')->name('testing');


//@Multiform without vue validation
Route::get('home-page','SubController\MultifieldController@index');
Route::post('save-page','SubController\MultifieldController@store');

//@Session Route
Route::get('session-home','SubController\SessionController@home');
Route::get('session-index','SubController\SessionController@index');
Route::post('session-store','SubController\SessionController@store');
Route::get('session-del','SubController\SessionController@remove');

//@Basic testing using Vue js
Route::get('vue-test','SubController\VueTestController@index');
Route::post('vue-store','SubController\VueTestController@store');