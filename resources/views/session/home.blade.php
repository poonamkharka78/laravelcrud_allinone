<!DOCTYPE html>
<html lang="en">
<head>
  <title>CrudLaravel</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<div class="container">
  <center><h2>Details</h2></center>
    <hr>
    <a href="/session-index" class="btn btn-success" style="float: left;">Add More</a>
    <hr>
    <br>
      <table class="table">
          <thead>
                <tr>
                  <th>Name</th>
                  <th>About</th>
                  <th>Actions</th>
                </tr>
          </thead>
              <tbody>
                @isset($data)
                  <tr>
                      <td>{{$data['name']}}</td>
                      <td>{{$data['about']}}</td>
                      <td>
                      <!--   <a href="" class="btn btn-info">Edit</a> -->
                        <a href="/session-del" class="btn btn-danger">Remove</a>
                      </td>
                  </tr>
                @endisset
                @empty($data)
                  
                    <p>
                      No data found
                    </p>
                  
                @endempty
              </tbody>
            </table>
         </div>
      </body>
    </html>

