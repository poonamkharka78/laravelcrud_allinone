<div class="container">
	<center>
	<form method="post" action="/session-store">
		<hr>
		<h3>Form Using Session</h3>
		@csrf
		@if(Session::has('msg'))
			<div class="alert" style="color: green;">{{Session::get('msg')}}</div>
		@endif
		@if(Session::has('fail'))
			<div class="alert">{{Session::get('fail')}}</div>
		@endif
		<br>
			<div class="row">
				<label>Name:</label>
					<input type="text" name="name">
			</div>
			<br>
			<div class="row">
				<label>About:</label>
					<input type="text" name="about">
			</div>
			<br>
		<button type="submit" name="submit" value="submit">Save!</button>
	</form>
	</center>
</div>