<!DOCTYPE html>
<html lang="en">
<head>
  <title>Laravel</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <center>
    <h2>Employee Details</h2>
    <hr/>
     <table class="table">
              <thead>
                <tr>
                  <th>Employee Name</th>
                  <th>Employee Department</th>
                  <th>Employee Date of Joining</th>
                </tr>
              </thead>
              <tbody>
                @if($data)
                  @foreach($data as $data)
                <tr>
                  <td>{{ $data->emp_name }}</td>
                  <td>{{ $data->department }}</td>
                  <td>{{ Carbon\Carbon::parse($data->date_of_joining)->format('d-m-Y') }} </td>
                </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
          </center>
        </div>
      </body>
    </html>
