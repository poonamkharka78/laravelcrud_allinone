<!DOCTYPE html>
<html lang="en">
<head>
  <title>CrudLaravel</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <center>
  <h2>Create Employee form</h2>
  <hr/> 
  <form class="form-horizontal" action="{{ url('update/'.$record->id) }}" method="post">
   @csrf
    <div class="form-group">
      <label class="control-label col-sm-2" for="email">Employee Name:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="emp_name" placeholder="Enter Employee Name" name="emp_name" value="{{ $record->emp_name }}">
      </div>
          @if($errors->first('emp_name'))
            <p style="color:red;font-weight:bold; text-align: left;">{{ $errors->first('emp_name')}}</p>
          @endif
    </div>
      <div class="form-group">
        <label class="control-label col-sm-2" for="pwd">Employee Department:</label>
        <div class="col-sm-10">          
          <input type="text" class="form-control" id="emp_department" placeholder="Enter Employee Department" name="department" value="{{ $record->department}}">
        </div>
            @if($errors->first('emp_department'))
             <p style="color:red;font-weight:bold;text-align: left;">{{ $errors->first('department')}}</p>
            @endif
      </div>
        <div class="form-group">        
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </div>
      </form>
    </center>
  </div>
</body>
</html>