<!DOCTYPE html>
<html lang="en">
<head>
  <title>CrudLaravel</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  
</head>
<body>

<div class="container">
  <center><h2>Employee Details</h2></center>
    <hr>
    <a href="/create" class="btn btn-success" style="float: left;">Add Employee</a>
    <hr>
    <br>
      @if ( Session::has('flash_message') )
          <div class="alert {{ Session::get('flash_type') }}">
            <h5>{{ Session::get('flash_message') }}</h5>
          </div>
  
      @endif
      @if ( Session::has('message') )
          <div class="alert {{ Session::get('flash_type') }}">
            <h5>{{ Session::get('message') }}</h5>
          </div>
  
      @endif
      @if ( Session::has('formData') )
          <div class="alert {{ Session::get('flash_type') }}">
            <h5>{{ Session::get('formData') }}</h5>
          </div>
  
      @endif
           <table class="table">
            
              <thead>
                <tr>
                  <th>Employee Name</th>
                  <th>Employee Department</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @if($response)
                  @foreach($response as $response)
                <tr>
                  <td>{{$response->emp_name}}</td>
                  <td>{{$response->department}}</td>
                  <td>
                    <a href="{{ url('edit/'.$response->id)}}" class="btn btn-info">Edit</a>
                    <a href="{{ url('delete/'.$response->id)}}" class="btn btn-danger">Remove</a>
                  </td>
                </tr>
                 @endforeach
              @endif
              </tbody>
            </table>
            {{$link}}
        </div>
      </body>
    </html>
    

