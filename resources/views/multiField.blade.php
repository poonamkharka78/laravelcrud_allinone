<!DOCTYPE html>
<html lang="en">
<head>
  <title>Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <style type="text/css">
    .myDiv{
      width: 20.666667%;
    }
  </style>
</head>
<body>
  <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header text-center">
    </div>
  </div>
</nav>
<div class="container" id="app">
  
  <h2>Customer Satisfaction Survey Form</h2>
  <hr>
  <form class="form-horizontal" action="/save-single-customer-data" method="post" @submit.prevent="onSubmit">
    @csrf
            
  @if (Session::has('mySession'))
          <div class="alert {{ Session::get('flash_type') }}">
            <h5>{{ Session::get('mySession') }}</h5>
          </div>
  
      @endif

   <div class="form-group">
      <label class="control-label col-sm-2" for="email">Name:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" v-model="form.name" id="name" placeholder="Enter Full Name" name="name">
         <span v-if="allerros.name" style="color: red;">@{{ allerros.name[0]}}</span> 
      </div>
    </div>
   <div class="form-group">
      <label class="control-label col-sm-2" for="contactNo">Contact No:</label>
      <div class="col-sm-10">          
        <input type="number" class="form-control" v-model="form.contactNo" id="contactNo" placeholder="Enter Contact Details" name="contactNo" />
       <span v-if="allerros.contactNo" style="color: red;">@{{ allerros.contactNo[0]}}</span> 
      </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="gender">Gender:</label>
          <div class="col-sm-5  myDiv">
            <label class="radio-inline ">
              <input type="radio"  name="gender" value="Male" v-model="form.gender">Male
            </label>
            <label class="radio-inline">
              <input type="radio" name="gender" value="Female" v-model="form.gender">Female
            </label>
            <label class="radio-inline">
              <input type="radio" name="gender" value="Others" v-model="form.gender">Others
            </label>
            <span v-if="allerros.gender" style="color: red;">@{{ allerros.gender[0]}}</span> 
          </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="email">Address:</label>
      <div class="col-sm-5">
        <input type="text" class="form-control" v-model="form.address1" id="address1" placeholder="Enter Address" name="address1">
          <span v-if="allerros.address1" style="color: red;">@{{ allerros.address1[0]}}</span> 
      </div>
      <div class="col-sm-5">
        <input type="text" class="form-control" v-model="form.address2" id="address2" placeholder="Enter Address" name="address2"> 
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="country">Country:</label>
        <div class="col-sm-5">
          <select id="country" class="form-control" v-model="form.country"  onchange="{{ url('customer-satisfaction-form') }}" name="country">
           
            @if($countries)
              @foreach($countries as $country)
                <option value="{{ $country->id }}">{{ $country->country_name }}</option>
              @endforeach
            @endif
          </select>
          <span v-if="allerros.country" style="color: red;">@{{ allerros.country[0]}}</span>
        </div>
        <div class="col-sm-5">
          <select id="state" name="state" v-model="form.state" class="form-control">
          </select>
           <span v-if="allerros.state" style="color: red;">@{{ allerros.state[0]}}</span> 
        </div>
      </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="city">City:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" v-model="form.city" id="city" placeholder="Enter City Name" name="city">
       <span v-if="allerros.city" style="color: red;">@{{ allerros.city[0]}}</span> 
      </div>
    </div>
     <div class="form-group">
      <label class="control-label col-sm-2" for="email">Email:</label>
      <div class="col-sm-10">
        <input type="email" class="form-control" v-model="form.email" id="email" placeholder="Enter email" name="email">
        <span v-if="allerros.email" style="color: red;">@{{ allerros.email[0]}}</span> 
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="description">Description:</label>
      <div class="col-sm-10">
        <textarea class="form-control" v-model="form.description" id="description" placeholder="Enter Description" name="description"></textarea> 
       <span v-if="allerros.description" style="color: red;">@{{ allerros.description[0]}}</span> 
      </div>
    </div>
   <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" name="submit" class="btn btn-success">Submit</button>
      </div>
    </div>
  </form>
  
</div>
</body>
</html>
<script src="http://www.codermen.com/js/jquery.js"></script>
<!--Vue validation Start-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>

<script type="text/javascript">
  const app = new Vue({
       el: '#app',
       data: {
           form: {
               name : '',
               contactNo : '',
               gender:'',
               address1:'',
               address2:'',
               country:'',
               state:'',
               city:'',
               email:'',
               description:'',
           },
           allerros: [],
           success : false,    
       },
       methods : {
           onSubmit() {
               dataform = new FormData();
               dataform.append('name', this.form.name);
               dataform.append('contactNo', this.form.contactNo);
               dataform.append('gender', this.form.gender);
               dataform.append('address1', this.form.address1);
               dataform.append('address2', this.form.address2);
               dataform.append('country', this.form.country);
               dataform.append('state',this.form.state);
               dataform.append('city', this.form.city);
               dataform.append('email', this.form.email);
               dataform.append('description', this.form.description);
               //console.log(this.form.name);
               axios.post('/save-single-customer-data', dataform).then( response => {
                   console.log(response);
                   this.allerros = [];
                   this.form.name = '';
                   this.form.contactNo = '';
                   this.form.gender = '';
                   this.form.address1 = '';
                   this.form.address2 = '';
                   this.form.country = '';
                   this.form.state = '';
                   this.form.city = '';
                   this.form.email = '';
                   this.form.description = '';
                    this.success = true;
               }).catch((error) => {
                        this.allerros = error.response.data.errors;
                        this.success = false;
                   });
           }
       }
   });

  $('#country').change(function(){
    var countryID = $(this).val();  
    //alert(countryID);  
    if(countryID){
        $.ajax({
          type:"GET",
          dataType:"json",
          url:"{{url('/state-list')}}?country_id="+countryID,
          success:function(res){               
            if(res){
                //alert(res);
                $("#state").empty();
                $("#state").append('<option>Select State</option>');
                $.each(res,function(key,value){
                   // alert(value.state_name);
                    $("#state").append('<option value="'+key+'">'+ value.state_name +'</option>');
                });
           
            }else{
               $("#state").empty();
            }
           }

        });
      }     
   }); 
</script>

<!--Vue Validation End-->

  
