@extends('layouts.app')

@section('content')
<div class="container">
	<h3>Image Upload Demo</h3>
	<hr>
	<form action="{{route('testing')}}" method="POST" enctype="multipart/form-data">
		@csrf

		@if ($message = Session::get('upload'))
			<div class="alert alert-success alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button>
			    	<strong>{{ $message }}</strong>
			    	
			</div>
			<div class="alert alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button>
			    <img style="width: 50%;height: 50%;" src="images/{{ Session::get('image') }}">
			</div>
			
		@endif

		@if($errors->first('image'))
            <p style="color:red;font-weight:bold; text-align: left;">{{ $errors->first('image')}}</p>
          @endif
			<div class="row">
				<div class="col-md-6">
					<input type="file" name="image" class="form-control">
				</div>
				<div class="col-md-6">
					<button type="submit" class="btn btn-success">Upload</button>
				</div>
			</div>
	</form>

</div>
@endsection

