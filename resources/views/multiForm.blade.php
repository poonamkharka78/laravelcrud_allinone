<!DOCTYPE html>
<html lang="en">
<head>
  <title>Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
  <style>
    .tab {
        display: none;
    }
    .tab1{
        display:block;
    }
</style>
</head>
<body>
<div class="container">
  <h2>Multi Form Example</h2>

  <hr/>
      @if ( Session::has('msg') )
          <div class="alert {{ Session::get('flash_type') }}">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <h5>{{ Session::get('msg') }}</h5>
          </div>
  
      @endif

  <form action="{{ url('storage') }}" method="post">
     {{ csrf_field() }}
     <div class="tab tab1">
        <div class="form-group">
          <label for="f_name">First Name:</label>
            <input type="text" class="form-control" id="f_name" placeholder="Enter First Name" name="f_name"  />
            @if($errors->first('f_name'))
              <p style="color:red;font-weight:bold;">{{ $errors->first('f_name')}}</p>
            @endif
        </div>
        <div class="form-group">
          <label for="l_name">Last Name:</label>
            <input type="text" class="form-control" id="l_name" placeholder="Enter Last Name" name="l_name" />
            @if($errors->first('l_name'))
              <p style="color:red;font-weight:bold;">{{ $errors->first('l_name')}}</p>
            @endif
        </div>
        <button type="button" name="btnFirst" class="btn back-btn" id="n1">Next</button>
     </div>
     <div class="tab tab2">
        <div class="form-group">
          <label for="mobile">Contact No:</label>
            <input type="number" class="form-control" id="mobile" placeholder="Enter Contact No" name="mobile">
            @if($errors->first('mobile'))
              <p style="color:red;font-weight:bold;">{{ $errors->first('mobile')}}</p>
            @endif
        </div>
        <div class="form-group">
          <label for="address">Address:</label>
            <input type="text" class="form-control" id="address" placeholder="Enter address" name="address">
            @if($errors->first('address'))
              <p style="color:red;font-weight:bold;">{{ $errors->first('address')}}</p>
            @endif
        </div>
        <button type="submit" name="btnsubmit" class="btn submit-btn">Submit</button>
     </div>
    
  </form>
</div>
</body>
</html>
<script type="text/javascript">
    $("#n1").click(function () {
        var clikedForm = $(this); // Select Form
        if ($('#f_name').val() == ''){
            alert('Enter First Name');
              $('.tab1').show();
              return false;
                }
                else{
                    $('.tab1').hide();
                    $('.tab2').show();

                }
            });
</script>
