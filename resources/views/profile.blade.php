@extends('layouts.app')
@section('content')
<div class="container">
	<h2>User Details</h2>
    	<table class="table">
            <thead>
		        <tr>
		        	<th>User Name</th>
		            <th>Bike</th>
		        </tr>
            </thead>
            	<tbody>
		           @forelse($products as $product)
		            		<tr>
		                  		<td>{{ $product->name }}</td>
		                  		<td>{{$product->Bike->bike_name}}</td>
		                	</tr>
		                	@empty
    							<p>No users</p>
		            @endforelse
		            
		        </tbody>
        </table>
	</div>
@endsection


