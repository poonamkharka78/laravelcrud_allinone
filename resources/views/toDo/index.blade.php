<!DOCTYPE html>
<html lang="en">
<head>
  <title>Laravel</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
  <div class="container">
    <center>
      <h2>ToDo's Task List</h2>
      <hr/> 
      <form class="form-horizontal" action="/todo-store" method="post">
       @csrf

        @if(Session::has('message'))
          <div class="alert">
            <h5 style="color: green;">{{Session::get('message')}}</h5>
          </div>
        @endif
        <div class="form-group">
          <label class="control-label col-sm-2">Task List</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="task" placeholder="Enter Task" name="task">
            </div>
            <div class="col-sm-2"> 
              <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </div>
      </form>
      <div class="alert" align="left">
       @if($records)
          @foreach($records as $record)
            <ul>
                <li>{{ $record->task }}  <a href="{{ url('delete-task/'.$record->id)}}"> x </a></li>
            </ul>
          @endforeach
        @endif

      </div>


    </center>
  </div>
  </body>
</html>