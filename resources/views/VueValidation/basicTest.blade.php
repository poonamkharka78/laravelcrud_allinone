<!DOCTYPE html>
<html lang="en">
<head>
  <title>Vue Validation Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
</head>
<body>

<div class="container" id="app">
  <h2>Test form</h2>
  <small>(Vue validation Example)</small>
  <form class="form-horizontal" action="{{url('vue-store')}}" @submit.prevent="onSubmit">
      @csrf
    <div class="form-group">
      <label class="control-label col-sm-2" for="name">Name:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="name" placeholder="Enter Name" name="Name" v-model="form.name">
        <span v-if="errors.name">@{{ errors.name[0] }}</span>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="email">Email:</label>
      <div class="col-sm-10">          
        <input type="email" class="form-control" id="email" placeholder="Enter Email" name="email" v-model="form.email">
        <span v-if="errors.email">@{{ errors.email[0] }}</span>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="email">Test:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="test" placeholder="Enter Test" name="test" v-model="form.test">
        <span v-if="errors.test">@{{ errors.test[0] }}</span>
      </div>
    </div>
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default">Submit</button>
      </div>
    </div>
  </form>

  <p>@{{ message }}</p>
  <button v-on:click="reverseMessage">Reverse Message</button>
</div>
</body>
</html>
<script type="text/javascript">
     var app = new Vue({
        el: '#app',
        data: {
            form: {
                name : '',
                email : '',
                test:'',
            },
            message: 'Hello Poonam!',
            errors: [],
            success : false,    
        },
        methods : {
            onSubmit: function(e) {


                formdata = new FormData();
                formdata.append('name', this.form.name);
                formdata.append('email', this.form.email);
                 formdata.append('test', this.form.test);
                console.log(e.target.action);


                axios.post(e.target.action, formdata).then( response => {
                    this.errors = [];
                    this.form.name = '';
                    this.form.email = '';
                    this.form.test = '';
                    this.success = true;
                } ).catch((error) => {
                         this.errors = error.response.data.errors;
                         this.success = false;
                    });
            },

            reverseMessage: function () {
                    this.message = this.message.split('').reverse().join('')
                  }
        

    }
  });
</script>
