<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\EmployeeDetails;
use Faker\Generator as Faker;

$factory->define(EmployeeDetails::class, function (Faker $faker) {
    return [
        'emp_name' => $faker->sentence(),
        'department' =>$faker->paragraph(rand(2,10),true),
    ];
});
